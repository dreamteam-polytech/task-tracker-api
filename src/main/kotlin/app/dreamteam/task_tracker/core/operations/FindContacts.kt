package app.dreamteam.task_tracker.core.operations

import app.dreamteam.task_tracker.core.entities.Contact

object FindContacts {
    data class Request(
        val page: Int,
        val pageSize: Int,
        val body: ContactRequest
    ) {
        data class ContactRequest(
            val name: String? = null,
            val tamtamId: Long? = null,
            val tamtamChatId: Long? = null
        )
    }

    data class Response(
        val page: Int,
        val pageSize: Int,
        val pagesTotal: Int,
        val body: List<Contact>
    )
}