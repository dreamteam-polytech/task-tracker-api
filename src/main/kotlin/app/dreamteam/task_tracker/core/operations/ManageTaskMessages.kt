package app.dreamteam.task_tracker.core.operations

import java.util.*

/**
 * Служит для управления связью между задачей и сообщениями.
 */
object ManageTaskMessages {
    sealed class Request(val taskId: UUID) {
        /** Для получения всех закрепленных за задачей сообщений. */
        class GetMessages(taskId: UUID) : Request(taskId)
        /** Для связывания сообщений с конкретной задачей. */
        class PinMessages(taskId: UUID, val chatIds_messageIds: List<Pair<Long, String>>) : Request(taskId)
        /** Для отвязывания сообщений от задачи. */
        class UnpinMessages(taskId: UUID, val messageIds: List<String>) : Request(taskId)
    }

    sealed class Response(val taskId: UUID) {
        /** Содержит все закрепленные за задачей сообщения. */
        class GetMessages(taskId: UUID, val messageIds: List<String>) : Response(taskId)

        /** Содержит все более не прикрепленные к задаче сообщения. */
        class PinMessages(taskId: UUID, val unpinnedMessageIds: List<String>) : Response(taskId)

        class UnpinMessages(taskId: UUID) : Response(taskId)
    }
}