package app.dreamteam.task_tracker.core.operations

sealed class OperationResult<T> {
    data class Success<T>(val value: T) : OperationResult<T>()
    data class Failure<T>(val cause: Throwable, val message: String = cause.localizedMessage) : OperationResult<T>()
}

fun <T> OperationResult<T>.unwrap(): T? = when (this) {
    is OperationResult.Failure -> null
    is OperationResult.Success -> value
}