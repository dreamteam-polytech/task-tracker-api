package app.dreamteam.task_tracker.core.operations

import app.dreamteam.task_tracker.core.entities.Contact
import app.dreamteam.task_tracker.core.entities.Task
import java.util.*

object FindTasks {
    data class Request(
        val page: Int,
        val pageSize: Int,
        val sender: Contact,
        val request: TaskRequest
    ) {
        data class TaskRequest(
            val title: String? = null,
            val reporter: Contact? = null,
            val assignee: Contact? = null,
            val status: Task.Status? = null,
            val description: String? = null,
            val uuid: UUID? = null
        )
    }

    data class Response(
        val page: Int,
        val pageSize: Int,
        val pagesTotal: Int,
        val tasks: List<Task>
    )
}