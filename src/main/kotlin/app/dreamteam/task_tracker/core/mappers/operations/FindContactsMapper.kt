package app.dreamteam.task_tracker.core.mappers.operations

import app.dreamteam.task_tracker.api.ContactDTO
import app.dreamteam.task_tracker.api.FindContactsDTO
import app.dreamteam.task_tracker.core.TTrackerMapperException
import app.dreamteam.task_tracker.core.mappers.BinaryMapper
import app.dreamteam.task_tracker.core.mappers.ListMapperImpl
import app.dreamteam.task_tracker.core.mappers.entities.ContactMapper
import app.dreamteam.task_tracker.core.operations.FindContacts

object FindContactsMapper {
    class RequestMapper : BinaryMapper<FindContactsDTO.Request, FindContacts.Request> {
        private val contactRequestMapper = ContactRequestMapper()

        override fun mapIn(input: FindContactsDTO.Request): FindContacts.Request {
            return FindContacts.Request(
                if (input.hasPage()) input.page else throw TTrackerMapperException(),
                if (input.hasPageSize()) input.pageSize else throw TTrackerMapperException(),
                if (input.hasBody()) contactRequestMapper.mapIn(input.body) else throw TTrackerMapperException()
            )
        }

        override fun mapOut(input: FindContacts.Request): FindContactsDTO.Request {
            val builder = FindContactsDTO.Request.newBuilder()
            builder.page = input.page
            builder.pageSize = input.pageSize
            builder.body = contactRequestMapper.mapOut(input.body)
            return builder.build()
        }
    }

    class ResponseMapper : BinaryMapper<FindContactsDTO.Response, FindContacts.Response> {
        private val contactMapper = ContactMapper()
        private val listMapper = ListMapperImpl(contactMapper)

        override fun mapIn(input: FindContactsDTO.Response): FindContacts.Response {
            return FindContacts.Response(
                if (input.hasPage()) input.page else throw TTrackerMapperException(),
                if (input.hasPageSize()) input.pageSize else throw TTrackerMapperException(),
                if (input.hasPagesTotal()) input.pagesTotal else throw TTrackerMapperException(),
                listMapper.mapIn(input.bodyList)
            )
        }

        override fun mapOut(input: FindContacts.Response): FindContactsDTO.Response {
            val builder = FindContactsDTO.Response.newBuilder()
            builder.page = input.page
            builder.pageSize = input.pageSize
            builder.pagesTotal = input.pagesTotal
            builder.addAllBody(listMapper.mapOut(input.body))
            return builder.build()
        }
    }

    private class ContactRequestMapper : BinaryMapper<ContactDTO, FindContacts.Request.ContactRequest> {
        override fun mapIn(input: ContactDTO): FindContacts.Request.ContactRequest {
            return FindContacts.Request.ContactRequest(
                if (input.hasName()) input.name else null,
                if (input.hasTamtamId()) input.tamtamId else null,
                if (input.hasTamtamChatId()) input.tamtamChatId else null
            )
        }

        override fun mapOut(input: FindContacts.Request.ContactRequest): ContactDTO {
            val builder = ContactDTO.newBuilder()
            input.name?.let { builder.name = it }
            input.tamtamId?.let { builder.tamtamId = it }
            input.tamtamChatId?.let { builder.tamtamChatId = it }
            return builder.build()
        }
    }
}