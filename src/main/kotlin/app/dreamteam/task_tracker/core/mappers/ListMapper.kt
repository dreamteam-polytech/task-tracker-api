package app.dreamteam.task_tracker.core.mappers

interface ListMapper<I, O>: BinaryMapper<List<I>, List<O>>

class ListMapperImpl<I, O>(
    private val mapper: BinaryMapper<I, O>
) : ListMapper<I, O> {
    override fun mapIn(input: List<I>): List<O> {
        return input.map { mapper.mapIn(it) }
    }

    override fun mapOut(input: List<O>): List<I> {
        return input.map { mapper.mapOut(it) }
    }
}