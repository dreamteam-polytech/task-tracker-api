package app.dreamteam.task_tracker.core.mappers.entities

import app.dreamteam.task_tracker.api.ContactDTO
import app.dreamteam.task_tracker.core.entities.Contact
import app.dreamteam.task_tracker.core.TTrackerMapperException
import app.dreamteam.task_tracker.core.mappers.BinaryMapper

class ContactMapper : BinaryMapper<ContactDTO, Contact> {
    override fun mapIn(input: ContactDTO): Contact {
        return Contact(
            if (input.hasName()) input.name else throw TTrackerMapperException(),
            if (input.hasTamtamId()) input.tamtamId else throw TTrackerMapperException(),
            if (input.hasTamtamChatId()) input.tamtamChatId else null
        )
    }

    override fun mapOut(input: Contact): ContactDTO {
        val builder = ContactDTO.newBuilder()
        builder.name = input.name
        builder.tamtamId = input.tamtamId
        input.tamtamChatId?.let { builder.tamtamChatId = it }
        return builder.build()
    }
}