package app.dreamteam.task_tracker.core.mappers.entities

import app.dreamteam.task_tracker.api.TaskDTO
import app.dreamteam.task_tracker.core.TTrackerMapperException
import app.dreamteam.task_tracker.core.entities.Task
import app.dreamteam.task_tracker.core.mappers.BinaryMapper
import java.util.*

class TaskMapper : BinaryMapper<TaskDTO, Task> {
    private val contactMapper = ContactMapper()
    private val statusMapper = TaskStatusMapper()

    override fun mapIn(input: TaskDTO): Task {
        return Task(
            if (input.hasTitle()) input.title else throw TTrackerMapperException(),
            if (input.hasReporter()) contactMapper.mapIn(input.reporter) else throw TTrackerMapperException(),
            if (input.hasAssignee()) contactMapper.mapIn(input.assignee) else null,
            if (input.hasStatus()) statusMapper.mapIn(input.status) else throw TTrackerMapperException(),
            if (input.hasDescription()) input.description else null,
            if (input.hasUuid()) UUID.fromString(input.uuid) else throw TTrackerMapperException()
        )
    }

    override fun mapOut(input: Task): TaskDTO {
        val builder = TaskDTO.newBuilder()
        builder.title = input.title
        builder.reporter = contactMapper.mapOut(input.reporter)
        input.assignee?.let { builder.assignee = contactMapper.mapOut(it) }
        builder.status = statusMapper.mapOut(input.status)
        input.description?.let { builder.description = it }
        builder.uuid = input.uuid.toString()
        return builder.build()
    }
}

class TaskStatusMapper : BinaryMapper<TaskDTO.Status, Task.Status> {
    override fun mapIn(input: TaskDTO.Status): Task.Status {
        return when (input) {
            TaskDTO.Status.TO_DO -> Task.Status.TO_DO
            TaskDTO.Status.IN_PROGRESS -> Task.Status.IN_PROGRESS
            TaskDTO.Status.IN_TEST -> Task.Status.IN_TEST
            TaskDTO.Status.DONE -> Task.Status.DONE
            else -> throw TTrackerMapperException("Received an unknown status which can't be mapped: " + input.name)
        }
    }

    override fun mapOut(input: Task.Status): TaskDTO.Status {
        return when (input) {
            Task.Status.TO_DO -> TaskDTO.Status.TO_DO
            Task.Status.IN_PROGRESS -> TaskDTO.Status.IN_PROGRESS
            Task.Status.IN_TEST -> TaskDTO.Status.IN_TEST
            Task.Status.DONE -> TaskDTO.Status.DONE
            else -> throw TTrackerMapperException("Mapping unknown status")
        }
    }
}