package app.dreamteam.task_tracker.core.mappers.operations

import app.dreamteam.task_tracker.api.ManageTaskMessagesDTO
import app.dreamteam.task_tracker.core.TTrackerMapperException
import app.dreamteam.task_tracker.core.mappers.BinaryMapper
import app.dreamteam.task_tracker.core.operations.ManageTaskMessages
import com.google.protobuf.Empty
import java.util.*

object ManageTaskMessagesMapper {
    class RequestMapper : BinaryMapper<ManageTaskMessagesDTO.Request, ManageTaskMessages.Request> {
        override fun mapIn(input: ManageTaskMessagesDTO.Request): ManageTaskMessages.Request {
            val taskUuid = if (input.hasTaskUuid()) input.taskUuid else throw TTrackerMapperException()
            return when (input.typeCase) {
                ManageTaskMessagesDTO.Request.TypeCase.GETMESSAGES -> {
                    ManageTaskMessages.Request.GetMessages(UUID.fromString(taskUuid))
                }
                ManageTaskMessagesDTO.Request.TypeCase.PINMESSAGES -> {
                    ManageTaskMessagesDTO.Request.PinMessages.ChatAndMessage.newBuilder()
                    ManageTaskMessages.Request.PinMessages(
                        UUID.fromString(taskUuid),
                        input.pinMessages.idsList.map { Pair(
                            if (it.hasChatId()) it.chatId else throw TTrackerMapperException(),
                            if (it.hasMessageId()) it.messageId else throw TTrackerMapperException()
                        )}
                    )
                }
                ManageTaskMessagesDTO.Request.TypeCase.UNPINMESSAGES -> {
                    ManageTaskMessages.Request.UnpinMessages(
                        UUID.fromString(taskUuid),
                        input.unpinMessages.messageIdsList
                    )
                }
                ManageTaskMessagesDTO.Request.TypeCase.TYPE_NOT_SET -> throw TTrackerMapperException()
                else -> throw TTrackerMapperException()
            }
        }

        override fun mapOut(input: ManageTaskMessages.Request): ManageTaskMessagesDTO.Request {
            val builder = ManageTaskMessagesDTO.Request.newBuilder()
            builder.taskUuid = input.taskId.toString()
            when (input) {
                is ManageTaskMessages.Request.GetMessages -> {
                    val subBuilder = ManageTaskMessagesDTO.Request.GetMessages.newBuilder()
                    builder.setGetMessages(subBuilder)
                }
                is ManageTaskMessages.Request.PinMessages -> {
                    val subBuilder = ManageTaskMessagesDTO.Request.PinMessages.newBuilder()
                    input.chatIds_messageIds.forEach { (chatId, messageId) ->
                        val idsBuilder = ManageTaskMessagesDTO.Request.PinMessages.ChatAndMessage.newBuilder()
                        idsBuilder.chatId = chatId
                        idsBuilder.messageId = messageId
                        subBuilder.addIds(idsBuilder)
                    }
                    builder.setPinMessages(subBuilder)
                }
                is ManageTaskMessages.Request.UnpinMessages -> {
                    val subBuilder = ManageTaskMessagesDTO.Request.UnpinMessages.newBuilder()
                    subBuilder.addAllMessageIds(input.messageIds)
                    builder.setUnpinMessages(subBuilder)
                }
            }
            return builder.build()
        }
    }

    class ResponseMapper : BinaryMapper<ManageTaskMessagesDTO.Response, ManageTaskMessages.Response> {
        override fun mapIn(input: ManageTaskMessagesDTO.Response): ManageTaskMessages.Response {
            val taskUuid = if (input.hasTaskUuid()) input.taskUuid else throw TTrackerMapperException()
            return when (input.typeCase) {
                ManageTaskMessagesDTO.Response.TypeCase.GETMESSAGES -> {
                    ManageTaskMessages.Response.GetMessages(
                        UUID.fromString(taskUuid),
                        input.getMessages.messageIdsList
                    )
                }
                ManageTaskMessagesDTO.Response.TypeCase.PINMESSAGES -> {
                    ManageTaskMessages.Response.PinMessages(
                        UUID.fromString(taskUuid),
                        input.pinMessages.unpinnedMessageIdsList
                    )
                }
                ManageTaskMessagesDTO.Response.TypeCase.UNPINMESSAGES -> {
                    ManageTaskMessages.Response.UnpinMessages(UUID.fromString(taskUuid))
                }
                ManageTaskMessagesDTO.Response.TypeCase.TYPE_NOT_SET -> throw TTrackerMapperException()
                else -> throw TTrackerMapperException()
            }
        }

        override fun mapOut(input: ManageTaskMessages.Response): ManageTaskMessagesDTO.Response {
            val builder = ManageTaskMessagesDTO.Response.newBuilder()
            builder.taskUuid = input.taskId.toString()
            when (input) {
                is ManageTaskMessages.Response.GetMessages -> {
                    val subBuilder = ManageTaskMessagesDTO.Response.GetMessages.newBuilder()
                    subBuilder.addAllMessageIds(input.messageIds)
                    builder.setGetMessages(subBuilder)
                }
                is ManageTaskMessages.Response.PinMessages -> {
                    val subBuilder = ManageTaskMessagesDTO.Response.PinMessages.newBuilder()
                    subBuilder.addAllUnpinnedMessageIds(input.unpinnedMessageIds)
                    builder.setPinMessages(subBuilder)
                }
                is ManageTaskMessages.Response.UnpinMessages -> {
                    builder.setUnpinMessages(Empty.getDefaultInstance())
                }
            }
            return builder.build()
        }
    }
}