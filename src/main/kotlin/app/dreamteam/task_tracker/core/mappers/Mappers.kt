package app.dreamteam.task_tracker.core.mappers

interface Mapper<I, O> {
    fun mapIn(input: I): O
}

interface BinaryMapper<I, O> : Mapper<I, O> {
    fun mapOut(input: O): I
}