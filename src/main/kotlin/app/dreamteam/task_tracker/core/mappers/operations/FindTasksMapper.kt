package app.dreamteam.task_tracker.core.mappers.operations

import app.dreamteam.task_tracker.api.FindTasksDTO
import app.dreamteam.task_tracker.api.TaskDTO
import app.dreamteam.task_tracker.core.TTrackerMapperException
import app.dreamteam.task_tracker.core.mappers.BinaryMapper
import app.dreamteam.task_tracker.core.mappers.ListMapperImpl
import app.dreamteam.task_tracker.core.mappers.entities.ContactMapper
import app.dreamteam.task_tracker.core.mappers.entities.TaskMapper
import app.dreamteam.task_tracker.core.mappers.entities.TaskStatusMapper
import app.dreamteam.task_tracker.core.operations.FindTasks
import java.util.*

object FindTasksMapper {
    class RequestMapper : BinaryMapper<FindTasksDTO.Request, FindTasks.Request> {
        private val contactMapper = ContactMapper()
        private val taskRequestMapper = TaskRequestMapper(contactMapper, TaskStatusMapper())

        override fun mapIn(input: FindTasksDTO.Request): FindTasks.Request {
            return FindTasks.Request(
                if (input.hasPage()) input.page else throw TTrackerMapperException(),
                if (input.hasPageSize()) input.pageSize else throw TTrackerMapperException(),
                if (input.hasSender()) contactMapper.mapIn(input.sender) else throw TTrackerMapperException(),
                if (input.hasBody()) taskRequestMapper.mapIn(input.body) else throw TTrackerMapperException()
            )
        }

        override fun mapOut(input: FindTasks.Request): FindTasksDTO.Request {
            val builder = FindTasksDTO.Request.newBuilder()
            builder.page = input.page
            builder.pageSize = input.pageSize
            builder.sender = contactMapper.mapOut(input.sender)
            builder.body = taskRequestMapper.mapOut(input.request)
            return builder.build()
        }
    }

    class ResponseMapper : BinaryMapper<FindTasksDTO.Response, FindTasks.Response> {
        private val taskMapper = TaskMapper()
        private val listMapper = ListMapperImpl(taskMapper)

        override fun mapIn(input: FindTasksDTO.Response): FindTasks.Response {
            return FindTasks.Response(
                if (input.hasPage()) input.page else throw TTrackerMapperException(),
                if (input.hasPageSize()) input.pageSize else throw TTrackerMapperException(),
                if (input.hasPagesTotal()) input.pagesTotal else throw TTrackerMapperException(),
                listMapper.mapIn(input.tasksList)
            )
        }

        override fun mapOut(input: FindTasks.Response): FindTasksDTO.Response {
            val builder = FindTasksDTO.Response.newBuilder()
            builder.page = input.page
            builder.pageSize = input.pageSize
            builder.pagesTotal = input.pagesTotal
            builder.addAllTasks(listMapper.mapOut(input.tasks))
            return builder.build()
        }
    }

    private class TaskRequestMapper(
        private val contactMapper: ContactMapper,
        private val statusMapper: TaskStatusMapper
    ) : BinaryMapper<TaskDTO, FindTasks.Request.TaskRequest> {
        override fun mapIn(input: TaskDTO): FindTasks.Request.TaskRequest {
            return FindTasks.Request.TaskRequest(
                if (input.hasTitle()) input.title else null,
                if (input.hasReporter()) contactMapper.mapIn(input.reporter) else null,
                if (input.hasAssignee()) contactMapper.mapIn(input.assignee) else null,
                if (input.hasStatus()) statusMapper.mapIn(input.status) else null,
                if (input.hasDescription()) input.description else null,
                if (input.hasUuid()) UUID.fromString(input.uuid) else null
            )
        }

        override fun mapOut(input: FindTasks.Request.TaskRequest): TaskDTO {
            val builder = TaskDTO.newBuilder()
            input.title?.let { builder.title = it }
            input.reporter?.let { builder.reporter = contactMapper.mapOut(it) }
            input.assignee?.let { builder.assignee = contactMapper.mapOut(it) }
            input.status?.let { builder.status = statusMapper.mapOut(it) }
            input.description?.let { builder.description = it }
            input.uuid?.let { builder.uuid = it.toString() }
            return builder.build()
        }
    }
}