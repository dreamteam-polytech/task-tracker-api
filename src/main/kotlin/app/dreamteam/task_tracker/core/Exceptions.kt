package app.dreamteam.task_tracker.core

open class TTrackerException(override val message: String, override val cause: Throwable? = null) : Exception(message, cause)
open class TTrackerIllegalStateException(message: String, cause: Throwable? = null) : TTrackerException(message, cause)
open class TTrackerIllegalAccessException(message: String, cause: Throwable? = null) : TTrackerException(message, cause)

open class TTrackerNetworkException(message: String, cause: Throwable? = null) : TTrackerException(message, cause)

open class TTrackerMapperException(message: String = "Required parameter was unspecified", cause: Throwable? = null) : TTrackerException(message, cause)