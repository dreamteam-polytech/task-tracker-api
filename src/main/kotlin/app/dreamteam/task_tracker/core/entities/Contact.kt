package app.dreamteam.task_tracker.core.entities

/**
 * Контакт. Сущность, которая осуществляет взаимодействие с задачами [Task].
 *
 * @property name имя
 * @property tamtamId уникальный идентификатор контакта в ТамТам
 * @property tamtamChatId null, или уникальный идентификатор чата с данным контактом в ТамТам
 */
data class Contact(
    val name: String,
    val tamtamId: Long,
    val tamtamChatId: Long?
)