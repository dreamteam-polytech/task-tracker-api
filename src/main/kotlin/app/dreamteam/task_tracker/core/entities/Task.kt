package app.dreamteam.task_tracker.core.entities

import app.dreamteam.task_tracker.core.TTrackerIllegalAccessException
import java.util.*

/**
 * Основная сущность таск-трекера. Отождествляет конкретную задачу.
 *
 * @property title название задачи
 * @property reporter лицо, создавшее задачу
 * @property assignee лицо, ответственное за исполнение
 * @property status текущее состояние задачи
 * @property description описание задачи
 * @property uuid уникальный идентификатор
 */
data class Task(
    val title: String,
    val reporter: Contact,
    val assignee: Contact? = null,
    val status: Status = Status.TO_DO,
    val description: String? = null,
    val uuid: UUID = UUID.randomUUID()
) {
    /**
     * Статус задачи.
     *
     * @property title видимый пользователю текст статуса
     */
    enum class Status(val title: String) {
        TO_DO("To Do"),
        IN_PROGRESS("In Progress"),
        IN_TEST("In Test"),
        DONE("Done");

        companion object {
            fun get(title: String): Status? = values().find { it.title == title }
        }
    }
}

fun Task.validateAccess(contact: Contact): Boolean = when (contact.tamtamId) {
    reporter.tamtamId -> true
    assignee?.tamtamId -> true
    else -> throw TTrackerIllegalAccessException("Only reporter and assignee have access to this task")
}