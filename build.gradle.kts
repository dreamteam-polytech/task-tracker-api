import com.google.protobuf.gradle.*

apply(plugin = "com.google.protobuf")
apply(plugin = "idea")

plugins {
    kotlin("kapt")
}

kapt {
    generateStubs = true
}

repositories {
    mavenCentral()
}

sourceSets {
    getByName("main").java.srcDirs("src/main/proto", "src/main/kotlin")
    getByName("test").java.srcDirs("src/test/kotlin")
}

val grpcVersion = "1.38.0"
val protobufVersion = "3.17.0"

dependencies {
    implementation("com.google.api.grpc:proto-google-common-protos:2.2.2")
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.4.3")
    implementation("javax.annotation:javax.annotation-api:1.3.2")
    implementation("io.grpc:grpc-netty:${grpcVersion}")
    implementation("io.grpc:grpc-protobuf:${grpcVersion}")
    implementation("io.grpc:grpc-stub:${grpcVersion}")
    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
    implementation("io.grpc:grpc-kotlin-stub:1.0.0")

    kapt("com.squareup.moshi:moshi-kotlin-codegen:1.11.0")
    kaptTest("com.squareup.moshi:moshi-kotlin-codegen:1.11.0")
    implementation("com.squareup.moshi:moshi:1.11.0")
    implementation("com.squareup.moshi:moshi-kotlin:1.11.0")
    implementation("com.squareup.moshi:moshi-adapters:1.11.0")
}

protobuf {
    protoc {
        // The artifact spec for the Protobuf Compiler
        artifact = "com.google.protobuf:protoc:${protobufVersion}"
    }
    plugins {
        id("grpc") {
            artifact = "io.grpc:protoc-gen-grpc-java:${grpcVersion}"
        }
        id("grpckt") {
            artifact = "io.grpc:protoc-gen-grpc-kotlin:1.1.0:jdk7@jar"
        }
    }
    generateProtoTasks {
        ofSourceSet("main").forEach {
            it.plugins {
                id("grpc") {}
                id("grpckt") {}
            }
        }
    }
}