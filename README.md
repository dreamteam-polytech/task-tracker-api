# TamTam Task Tracker - API

This repository uses the gRPC framework and serves as a link between [Task Tracker Service](https://gitlab.com/dreamteam-polytech/task-tracker) and [Task Tracker Bot](https://gitlab.com/dreamteam-polytech/task-tracker-bot). 